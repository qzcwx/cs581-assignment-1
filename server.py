from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
import socket
import xmlrpclib
import hashlib
import random
class Node:
    def init(self, name, ID, m, IDtoName):
        self.n = ID
        self.m = m
        self.data = dict()
        self.start = [0]*self.m
        self.node = [0]*self.m
        self.name = name
        self.IDtoName = IDtoName
        self.predecessor = None

        for i in xrange(self.m):
            self.start[i] = (2**i + self.n)%2**m
        print 'self.start', self.start
        
    def join(self, deployedNode):
        if deployedNode:
            # randomly select a node
            np = random.choice(deployedNode)
            print 'np', np
            self.init_fingeer_table(np)
            self.update_others();
        else:
            # first node in the network
            for i in xrange(self.m):
                self.node[i] = self.n
            self.predecessor = self.n
            print 'self.node', self.node
        deployedNode.append(self.n)
        print 'join', deployedNode
        return deployedNode
    
    def init_fingeer_table(self, np):
        print 'init_fingeer_table 0'
        print 'self.start[0]', self.start[0]
        self.node[0] = self.IDtoInstance(np).find_successor(self.start[0])
        print 'succ', self.node[0]
        self.predecessor = self.IDtoInstance(self.node[0]).predecessor
        self.IDtoInstance(self.node[0]).predecessor = self.n
        for i in xrange(self.m-1):
            if start[i+1] >= self.n and start[i+1] < self.node[i]:
                self.node[i+1] = self.node[i]
            else:
                self.node[i+1] = self.IDtoInstance(np).find_successor(self.start[i+1])
        print 'init_fingeer_table 1'
                
    def update_others(self):
        for i in xrange(m):
            p = find_predecessor(n-2**i)
            self.IDtoInstance(p).update_finger_table(n,i)

    def update_finger_table(self, s, i):
        if s >= n and s < self.node[i] :
            self.node[i] = s
            p = self.predecessor
            self.IDtoInstance(p).update_finger_table(s, i)
        
    def find_successor(self, ID):
        # find ID's successor
        print 'find_successor start, ID', ID
        print 'self.node', self.node
        np = self.find_predecessor(ID)
        print 'find_predecessor end, np', np
        return self.IDtoInstance(np).node[0]
        
    def find_predecessor(self, ID):
        # find ID's predecessor
        print 'find_predecessor(ID) start, ID', ID
        np = self.n
        print 'np', np
        print 'self.IDtoInstance(np).node[0]', (self.IDtoInstance(np)).node[0]
        print 'start loop'
        while not ( np < ID and ID <= self.IDtoInstance(np).node[0]):
            np = self.IDtoInstance(np).clost_preceding_finger(ID)
            print 'np', np
            print 'self.IDtoInstance(np).node[0]', (self.IDtoInstance(np)).node[0]
        return np

    # def get_successor(self):
    #     return self.node[0]
    
    def clost_preceding_finger(self,ID):
        # return the closest finger preceding ID
        print 'clost_preceding_finger, ID', ID
        for i in xrange(self.m-1,-1,-1):
            if self.n < self.node[i] and self.node[i]< ID:
                return self.node[i]
        return self.n
        
    def compRingLocation(self, key):
        return int('0x'+hashlib.sha1(key).hexdigest(),0)%self.ringsize

    def IDtoInstance(self, ID):
        # print '\nIDtoInstance 0'
        # print str(ID), '->', self.IDtoName[str(ID)]
        # print 'IDtoInstance 1\n'
        if self.n == ID:
            return self
        else:
            return xmlrpclib.ServerProxy('http://{}:8080'.format(self.IDtoName[str(ID)]))
    
# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

    
# Create server
print socket.gethostname()
server = SimpleXMLRPCServer((socket.gethostname(), 8080),
                            requestHandler=RequestHandler,
                            allow_none=True,
                            # bind_and_activate=False,
    )
server.register_introspection_functions()
server.register_instance(Node())
# server.allow_reuse_address = True

# Run the server's main loop
try:
    server.serve_forever()
finally:
    server.server_close()
