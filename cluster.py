# cluster class contains several nodes
from node import Node 
import hashlib
import rpyc
import pdb
import xmlrpclib
import subprocess
import os
import sys
import random

class Cluster:
    def __init__(self):
        self.nodeNameList = []
        self.deployedNode = []
        self.m = 3             # ringe size is 2^m
        self.ringsize = 2**self.m
        self.machineListFile = 'machines.lst.short'
        # self.machineListFile = 'machines.lst.local'
        self.readMachineList()
        self.IDtoName = dict()  # maps hash value back to name avoid decoding
        # initialize IDtoName, collect ID for all nodes
        for n in self.nodeNameList:
            ID = self.compRingLocation(n)
            if ID in self.IDtoName:
                print 'ERROR: duplicate in self.IDtoName'
            else:
                self.IDtoName[str(ID)] = n
                
    def deploy(self):
        # initialize each node, join in the ring all at once, there is
        # no associated update
        allID = sorted(self.IDtoName.keys())
        for i in xrange(len(allID)):
            ID = allID[i]
            name = self.IDtoName[str(ID)]
            print name
            if (i == len(allID)-1): # wrap around
                succ = allID[0]
            else:
                succ = allID[i+1]
            n = xmlrpclib.ServerProxy('http://{}:8080'.format(name))
            print 'ID', int(ID)
            n.init(name, int(ID), self.m, self.IDtoName)
            self.deployedNode = n.join(self.deployedNode)
            print self.deployedNode

        # disperse the key over each node, loop over datasets

    def readMachineList(self):
        # read machine list from file
        with open(self.machineListFile, 'r') as f:
            for line in f:
                line = line.rstrip('\r\n')
                self.nodeNameList.append(line)

    def compRingLocation(self, key):
        return int('0x'+hashlib.sha1(key).hexdigest(),0)%self.ringsize

    def put(self, source, key, value):
        # stores the value "value" with the key "key" from the specified
        # data source
        compkey = '{}:{}'.format(source,key)
        print compkey
        hash = self.compRingLocation(compkey)
        print 'hashvalue',hash
        
        # lookup the successor responsible for key k
        # start from an arbitrary node
        n = random.choice(self.nodeNameList)
        
