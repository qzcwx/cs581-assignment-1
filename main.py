# demo for testing Source-aware key-value store, Assignment 1 from CS581, in Fall 2013
import sys

prefix = "data/"
sourceList = ["sdsc-http.txt"]


c = Cluster()
c.deploy()

# parse in put
if (sys.argv[1]=="put"):
    # put source key value:
    # stores the value "value" with the key "key" from the specified data source
    print 'put'
    source = sys.argv[2]
    print 'source', source
    key = sys.argv[3]
    print 'key', key
    value = sys.argv[4]
    print 'value', value
else:
    print "USAGE: \"put source key value\"  or \"get source key\" or  \"del source key\""
