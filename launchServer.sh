#!/bin/bash
# launch server.py on each worker node

if [ "x$1" = "x" ]; then
    echo "USAGE: launchServer.sh 0[close] 1[launch]"
    exit 1
fi


# for i in `cat machines.lst.local`
if [ $1 -eq 1 ]
then
    echo Process ID $$
    for i in `cat machines.lst.short`
    do
        if [ $i = "localhost" ]
        then
            ssh $i "cd /home/chenwx/Study/CSU/Fall-2013/CS581/assignment1; python -u server.py" 1>/tmp/$$.$i.out 2> /tmp/$$.$i.err &
        else
            # ssh $i "cd /s/chopin/b/grad/chenwx/sched/cs581-a1; python -u server.py" 1>/tmp/$$.$i.out 2> /tmp/$$.$i.err &
            ssh $i "cd /s/chopin/b/grad/chenwx/sched/cs581-a1; python -u server.py" 1>/tmp/$i.out 2> /tmp/$i.err &
        fi
        echo $i launched
    done
elif [ $1 -eq 0 ]
then
    for i in `cat machines.lst.short`
    do
        ssh $i "pkill -U chenwx python" &
        echo $i closed
    done    
fi
