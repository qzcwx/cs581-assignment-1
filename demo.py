# demo for testing Source-aware key-value store, Assignment 1 from CS581, in Fall 2013

import subprocess
import sys
from cluster import Cluster
import string

prefix = "data/"
sourceList = ["sdsc-http.txt.short"]

c = Cluster()
c.deploy()

# dumps all data
for s in sourceList:
    with open(prefix+s, 'r') as f:
        for line in f:
            # create key
            line = line.rstrip('\r\n')
            key = line.split("(")[0].strip()
            c.put(s, key, line)


# # promote inputs
# while True:
#     cmd = raw_input("Enter Command: ")
#     word = string.split(cmd, "\"")
#     word =filter(lambda name: name.strip(), word)       # remove whitespaces
#     print word
#     if word[0] == 'put':
#         pass
#     else if word[0] == 'get':
#         pass
#     else if word[0] == 'del':
#         pass
#     else:
#         print 'WRONG COMMAND!!'
        

# # initialize storage nodes
# try:
#     # staging data set, one line at a time
#     for s in sourceList:
#         with open(prefix+s, 'r') as f:
#             for line in f:
#                 # create key
#                 key = 'key-1'
#                 line = line.rstrip('\r\n')
#                 # print 'line', line
#                 command = "python main.py put \"{}\" \"{}\" \"{}\"".format(s, key, line)
#                 subprocess.Popen( command , shell=True)
# except:
#     print "Unexpected error:", sys.exc_info()[0]
#     exit()
    
# # c.close()
